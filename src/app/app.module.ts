import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { GaleriaComponent } from './galeria/galeria.component';
import { NavbarComponent } from './navbar/navbar.component';
import { ListaImagenesComponent } from './galeria/lista-imagenes.component';
import { ImagenComponent } from './galeria/imagen.component';
import { ImagenService } from './services/imagen.service';
import { DetalleImagenComponent } from './galeria/detalle-imagen.component';
import { ContactoComponent } from './galeria/contacto.component';
import { AcercadeComponent } from './galeria/acercade.component';

@NgModule({
  declarations: [
    AppComponent,
    GaleriaComponent,
    NavbarComponent,
    ListaImagenesComponent,
    ImagenComponent,
    DetalleImagenComponent,
    ContactoComponent,
    AcercadeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [ImagenService],
  bootstrap: [AppComponent]
})
export class AppModule { }
