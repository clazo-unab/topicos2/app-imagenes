import { Component, OnInit, Input } from '@angular/core';
import { Imagen} from '../models/imagen';

@Component({
    selector: 'app-imagen',
    templateUrl: './imagen.component.html',
    styles: [`/*img es para indicar que los cambios aplicarán solo para elementos de tipo imagen*/
  img {
      /* creación de sobras considerando navegadores basados en webkit*/
      -webkit-box-shadow: 0px 1 px 6px rgba(0, 0, 0, 0.75);
      /* creación de sobras considerando navegador Mozilla*/
      -moz-box-shadow: 0px 1 px 6px rgba(0, 0, 0, 0.75);
      /* creación de sobras para navegadores modernos*/
      box-shadow: 0px 1 px 6px rgba(0, 0, 0, 0.75);
      /* establece un margen inferior de 20 pixeles*/
      margin-bottom: 20px;
  
  }
  
  /*indica que los cambios aplicarán para elementos de tipo imagen solo cuando el mouse pase por sobre ellos*/
  img:hover {
      /* se utiliza para aplicar un filtro por sobre el elemento, en este caso un filtro gris*/
      filter: gray;
      /* indica la velocidad con la que aplicará el filtro. webkit indica que se considera solo para navegadores basados en webkit*/
      -webkit-filter: grayscale(1);
  }
  
  .img1 {
      -webkit-filter: brightness(2);
  }
  
  .img2 {
      -webkit-filter: invert();
  }
  
  .img3 {
      -webkit-filter: contrast(3);
  }
  
  .img4 {
      -webkit-filter: sepia();
  }`
    ]
})
export class ImagenComponent implements OnInit {
    
    @Input() imagen!: Imagen;

    constructor() { }

    ngOnInit(): void {
    }

}
