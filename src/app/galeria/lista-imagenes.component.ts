import { Component, OnInit } from '@angular/core';
import { Imagen } from '../models/imagen';
import { ImagenService } from '../services/imagen.service';

@Component({
    selector: 'app-lista-imagenes',
    templateUrl: './lista-imagenes.component.html',
    styles: [
    ]
})
export class ListaImagenesComponent implements OnInit {

    imagenes: Imagen[] = [];
    selectedImagen: Imagen | undefined;

    constructor(private imagenService: ImagenService) { } 

    ngOnInit(): void {

        this.imagenes = this.imagenService.getImagenes();
    }

    onSelect(imagen: Imagen) {
        this.selectedImagen = imagen;
    }

}
