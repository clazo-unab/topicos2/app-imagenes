import { Component, Input, OnInit } from '@angular/core';
import { Imagen } from '../models/imagen';

@Component({
    selector: 'app-detalle-imagen',
    templateUrl: './detalle-imagen.component.html',
    styles: [`
    
    .detalle-imagen {
        -webkit-filter: contrast(3);
    }`
    ]
})
export class DetalleImagenComponent implements OnInit {

    @Input()
    selectedImagen!: Imagen;

    constructor() { }

    ngOnInit(): void {
    }

}
