import { Injectable } from '@angular/core';
import { Imagen } from '../models/imagen';

@Injectable({
  providedIn: 'root'
})
export class ImagenService {
    imagenes: Imagen[] = [
        new Imagen('1', 'imagen 1', 'descripcion de la imagen 1', 'https://nick-intl.mtvnimages.com/uri/mgid:file:gsp:scenic:/international/nickelodeon.es/images/bobv2home1280x20.png?height=360&width=480&matte=true&crop=true', 'https://www.enter.co/wp-content/uploads/2020/10/Kamp_Koral_Press_Art-768x432.jpg'),
        new Imagen('2', 'imagen 2', 'descripcion de la imagen 2', "https://nick-intl.mtvnimages.com/uri/mgid:file:gsp:scenic:/international/games/spongebob-saves-the-day/es-eu/SPONGEBOB_SQUAREPANTS_SB_SAVES_THE_DAY_Castillian_Spanish_thumbnails_1024x768.jpg?quality=0.75&height=360&width=480&matte=true&crop=true", 'https://www.enter.co/wp-content/uploads/2020/10/Kamp_Koral_Press_Art-768x432.jpg'),
        new Imagen('3', 'imagen 3', 'descripcion de la imagen 3', "https://imageresizer.static9.net.au/MEW_Xcl-6L5Q5EFtxLbXJ7EBM3Y=/600x0/smart/https%3A%2F%2Fs3-ap-southeast-2.amazonaws.com%2Fnine-tvmg-images-prod%2F66%2F05%2F94%2F660594_p349976_b_h3_aa.jpg", 'https://www.enter.co/wp-content/uploads/2020/10/Kamp_Koral_Press_Art-768x432.jpg'),
        new Imagen('4', 'imagen 4', 'descripcion de la imagen 4', "http://naciongrita.com.mx/wp-content/uploads/2019/07/rockos-modern-life.jpg", 'https://www.enter.co/wp-content/uploads/2020/10/Kamp_Koral_Press_Art-768x432.jpg'),
        new Imagen('5', 'imagen 5', 'descripcion de la imagen 5', 'https://nick-intl.mtvnimages.com/uri/mgid:file:gsp:scenic:/international/nickelodeon.es/images/bobv2home1280x20.png?height=360&width=480&matte=true&crop=true', 'https://www.enter.co/wp-content/uploads/2020/10/Kamp_Koral_Press_Art-768x432.jpg'),
        new Imagen('6', 'imagen 6', 'descripcion de la imagen 6', "https://nick-intl.mtvnimages.com/uri/mgid:file:gsp:scenic:/international/games/spongebob-saves-the-day/es-eu/SPONGEBOB_SQUAREPANTS_SB_SAVES_THE_DAY_Castillian_Spanish_thumbnails_1024x768.jpg?quality=0.75&height=360&width=480&matte=true&crop=true", 'https://www.enter.co/wp-content/uploads/2020/10/Kamp_Koral_Press_Art-768x432.jpg'),
        new Imagen('7', 'imagen 7', 'descripcion de la imagen 7', "https://imageresizer.static9.net.au/MEW_Xcl-6L5Q5EFtxLbXJ7EBM3Y=/600x0/smart/https%3A%2F%2Fs3-ap-southeast-2.amazonaws.com%2Fnine-tvmg-images-prod%2F66%2F05%2F94%2F660594_p349976_b_h3_aa.jpg", 'https://www.enter.co/wp-content/uploads/2020/10/Kamp_Koral_Press_Art-768x432.jpg'),
        new Imagen('8', 'imagen 8', 'descripcion de la imagen 8', "http://naciongrita.com.mx/wp-content/uploads/2019/07/rockos-modern-life.jpg", 'url')
        ];

  constructor() {}
  getImagenes() {
      return this.imagenes;
  }
}
