import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GaleriaComponent } from './galeria/galeria.component';
import { ContactoComponent } from './galeria/contacto.component';
import { AcercadeComponent } from './galeria/acercade.component';

const routes: Routes = [
    { path: '', redirectTo: '/galeria', pathMatch: 'full' },
    { path: 'galeria', component: GaleriaComponent },
    { path: 'contacto', component: ContactoComponent },
    { path: 'acercade', component: AcercadeComponent },
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
